import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  formGroup: FormGroup;
  successMessage : boolean;
  errorMessage: boolean;
  
  constructor(private auth :AuthService) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      Email: new FormControl("", [
        Validators.required
        // Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ])
    });
  }

  onSubmit() {

    const email = this.formGroup.value.Email;
    this.auth.forgotPassword(email)
   .subscribe(
     result => {
       this.successMessage = true;
     },
     error => {
       this.errorMessage = error.message;
       console.log(error);
     });
     }


}
