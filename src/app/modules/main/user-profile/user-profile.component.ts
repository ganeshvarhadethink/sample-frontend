import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../auth/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"]
})
export class UserProfileComponent implements OnInit {
  formGroup: FormGroup;
  formGroupPassword: FormGroup;
  userEmail: string;
  errorMessage: string = "";
  updateuserStatus: string = "";
  updatePasswordStatus: string = "";
  userPassword: string;
  IsPassword: boolean = false;
  passwordNotMatch: boolean = false;
  newPassword: string;
  confirmPassword: string;
  successMessage: string;
  result: any;

  constructor(private auth: AuthService, private router: Router) {
    this.auth.getUserLoginInfo().subscribe(res => {
      // console.log("res", JSON.stringify(res))
      this.userEmail = res.attributes.email;
      this.userPassword = "12345678";
      this.newPassword = "";
      this.confirmPassword = "";
    });
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      Email: new FormControl("", [
        Validators.required,
        Validators.pattern(
          /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        )
      ]),
      Password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ])
    });
    this.formGroupPassword = new FormGroup({
      Password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      NewPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      ConfirmPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ])
    });
  }

  checkPasswordValidation() {
    if (
      this.formGroupPassword.value.NewPassword !=
      this.formGroupPassword.value.ConfirmPassword
    ) {
      this.passwordNotMatch = true;
    } else {
      this.passwordNotMatch = false;
    }
  }
  // async onSubmit() {
  //   const email = this.formGroup.value.Email;
  //   this.updateuserStatus = await this.auth.updateProfile(email);
  //   if (this.updateuserStatus == "SUCCESS")
  //     setTimeout(() => {
  //       this.router.navigate(["/"]);
  //     }, 4000);
  // }

  // public changePassword() {
  //   const oldPassword = this.formGroupPassword.value.Password,
  //     newPassword = this.formGroupPassword.value.NewPassword;
  //   this.auth.changePassword(oldPassword, newPassword).subscribe(
  //     result => {
  //       // console.log("result", result)
  //       if(result === "SUCCESS"){
  //       // console.log("result success", result)
  //         this.successMessage = "Password update successfully";
  //         setTimeout(() => {
  //           this.router.navigate(["/"]);
  //         }, 4000);
  //       }
  //       else{
  //         // console.log("result error", result)
  //         this.successMessage = result.message;
  //       }
  //     },
  //     // error => {
  //     //   this.errorMessage = error.message;
  //     //   console.log(error);
  //     // }
  //   );
  // }
  async onSubmit() {
    const email = this.formGroup.value.Email;
    this.updateuserStatus = await this.auth.updateProfile(email);
    if (this.updateuserStatus == "SUCCESS")
      setTimeout(() => {
        this.router.navigate(["/"]);
      }, 4000);
  }

  async changePassword() {
    const oldPassword = this.formGroupPassword.value.Password,newPassword = this.formGroupPassword.value.NewPassword;
    this.result = await this.auth.changePassword(oldPassword, newPassword);
  }

  
  updatePassword() {
    this.IsPassword = true;
  }
  updateEmail() {
    this.IsPassword = false;
  }
}