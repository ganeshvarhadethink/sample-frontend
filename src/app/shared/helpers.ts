const convertComponentConfigToFroalaElements = (jsonContent: any): any => jsonContent.elements ? jsonContent.elements.reduce((acc, element) => {
  let froalaElement = null;

  switch(element.type) {
    case 'image':
      froalaElement = { src: element.src, class: element.class, style: element.style };
      break;
    case 'button':
      froalaElement = { innerHTML: element.htmlContent };
      break;
    case 'text':
      froalaElement = element.htmlContent;
      break;
  }

  return {
    ...acc,
    [element.name]: froalaElement
  };
}, {}) : {};

const textOptions: Object = {
  toolbarInline: true
};

const imgOptions: Object = {
  angularIgnoreAttrs: ['ng-reflect-froala-editor', 'ng-reflect-froala-model'],
  immediateAngularModelUpdate: true,
  imageTextNear: false,
  charCounterCount: true,
  imageEditButtons: ['imageReplace', 'imageSize', 'imageLink', 'linkRemove'],

  // Set max image size to 5MB.
  imageMaxSize: 5 * 1024 * 1024,

  // Allow to upload PNG and JPG.
  imageAllowedTypes: ['jpeg', 'jpg', 'png', 'svg']
};

const generateEditorsFromFroalaNames = (jsonContent: any): any => jsonContent.elements ? jsonContent.elements.reduce((acc: any, element: any) => {
  let editor = null;

  switch (element.type) {
    case 'image':
      editor = { ...imgOptions };
      break;
    case 'button':
      editor = null;
      break;
    case 'text':
      editor = { ...textOptions };
      break;
  }

  return {
    ...acc,
    [element.name]: editor
  };
}, {}) : {};

const convertFroalaElementsToComponentConfig = (jsonContent: any, froalaElements: any): any => {
  const updatedJsonComponentElements = jsonContent.elements.map((element) => {
    switch (element.type) {
      case 'image':
        return {
          ...element,
          class: froalaElements[element.name].class,
          style: froalaElements[element.name].style
        };
      case 'text':
        return {...element, htmlContent: froalaElements[element.name]};
      case 'button':
        return {...element, htmlContent: froalaElements[element.name].innerHTML};
    }
  });

  return {...jsonContent, elements: updatedJsonComponentElements};
};

const getElementType = (jsonContent, froalaElementName: any): string => {
  const findElement = jsonContent.elements.find(element => element.name === froalaElementName);

  return findElement.type;
};

const mapImagesToFiles = (jsonContent: any, images: any): any => {
  return images.map(image => {
    const findPath = jsonContent.elements.find(element => element.name === image.name).src;
    const fileName = getFileName(findPath);
    const filePath = getFilePath(findPath);

    return {
      name: image.name,
      base64: image.base64,
      fileName,
      filePath
    }
  });
};

const getFilePath = (fullPath: string) => {
  const lastSlashIndex = fullPath.lastIndexOf('/');

  return `src${fullPath.slice(0, lastSlashIndex)}/`;
};

const getFileName = (fullPath: string) => {
  const lastSlashIndex = fullPath.lastIndexOf('/');

  return fullPath.slice(lastSlashIndex + 1);
};

export {
  convertComponentConfigToFroalaElements,
  convertFroalaElementsToComponentConfig,
  generateEditorsFromFroalaNames,
  getElementType,
  mapImagesToFiles
};
