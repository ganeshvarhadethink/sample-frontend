const SERVER_URL = 'http://localhost:9080';

const GIT_URL = `${SERVER_URL}/api/github/`;

export {
  GIT_URL
};
